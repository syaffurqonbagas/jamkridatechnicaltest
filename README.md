# jamkridaTechnicalTest

## Getting started

Hallo ini merupakan hasil dari technical test jamkrida atas nama Bagas Syaffurqon. Saya menggunakan Express.js sebagai BackEnd FrameWork dan React.JS sebagai FrontEnd FrameWork.

## Aplikasi Yang Di Butuhkan

1. Node.js
2. Monggo DB

## Tata Cara Instalasi

1. Insatall node.js LTS dan Monggo DB menggunakan link di bawah: -https://nodejs.org/en/download -https://www.mongodb.com/try/download/community

2. Tahap 1 untuk test tinggal memanggil soal di dalam terminal folder dengan mengetikan command di bawah lalu di enter:
   node soal1.js

3. Tahap 2 masing-masih memiliki 2 folder yaitu express-api sebagai backend framework dan react-frontend sebagai frontend framework

4. Di tiap folder diatas harus melakukan instalasi dengan memasukan command :

```
  npm install
```

5. Setelah selesai jalankan terlebih dahulu express-api dengan menggunakan command:

```
   npm run dev
```

6. Kemudian jalankan react-frontend dengan menggunakan comman:

```
   npm start
```

7. React frontend berjalan di port localhost 3000 sedangankan untuk API local pada port localhost 8000

8. Produk api.postman_collection adalah dokumentasi dari API yang saya gunakan.

9. DataBase Masih kosong sehingga data harus di tambahkan secara manual.
