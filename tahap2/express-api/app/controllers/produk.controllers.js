const db = require("../models");
const Produk = db.produk;

exports.create = (req, res) => {
  Produk.create(req.body)
    .then(() => {
      res.send({ message: "data berhasil di tambahkan" });
    })
    .catch((err) => res.status(500).send({ message: err.message }));
};

exports.findAll = (req, res) => {
  Produk.find()
    .then((data) => res.send(data))
    .catch((err) => res.status(500).send({ message: err.message }));
};

exports.delete = (req, res) => {
  const id = req.params.id;
  Produk.findByIdAndDelete(id)
    .then((data) => {
      if (!data) {
        res.status(404).send({ message: "Tidak dapat menghapus Data" });
      } else {
        res.send({ message: "Data Berhasil Di Hapus" });
      }
    })
    .catch((err) => res.status(500).send({ message: err.message }));
};
exports.show = (req, res) => {
  const id = req.params.id;

  Produk.findById(id)
    .then((data) => res.send(data))
    .catch((err) => res.status(500).send({ message: err.message }));
};
exports.search = (req, res) => {
  const keywords = {};
  if (req.query.keywords) {
    keywords = { name: { $regex: req.query.keywords } };
  }
  Produk.find(keywords, "name")
    .then((data) => res.send(data))
    .catch((err) => res.status(500).send({ message: err.message }));
};

exports.update = (req, res) => {
  const id = req.params.id;
  Produk.findByIdAndUpdate(id, req.body, { userFindAndModify: false })
    .then((data) => {
      if (!data) {
        res.status(404).send({ message: "Tidak dapat mengUpdate Data" });
      } else {
        res.send({ message: "Data Berhasil Update" });
      }
    })
    .catch((err) => res.status(500).send({ message: err.message }));
};
