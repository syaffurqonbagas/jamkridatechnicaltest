module.exports = (app) => {
  const product = require("../controllers/produk.controllers");
  const r = require("express").Router();

  r.get("/", product.findAll);
  r.post("/", product.create);
  r.get("/:id", product.show);
  r.get("/", product.search);
  r.put("/:id", product.update);
  r.delete("/:id", product.delete);

  app.use("/produk", r);
};
