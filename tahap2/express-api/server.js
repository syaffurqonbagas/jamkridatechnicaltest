const express = require("express");
const cors = require("cors");
const db = require("./app/models/index");

const app = express();

const corsOption = {
  origin: "*",
};

//register cors middleware

app.use(cors(corsOption));
app.use(express.json());

//db connection

const mongooseConfig = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
};

db.mongoose
  .connect(db.url, mongooseConfig)
  .then(() => console.log("DataBase Connected"))
  .catch((err) => {
    console.log("DataBase Not Connected", err);
    process.exit();
  });

//memanggil routes produk

require("./app/routes/produk.routes")(app);

//making rooutes

app.get("/", (req, res) => {
  res.json({ message: "hello yuhu aku bagas" });
});

const PORT = process.env.PORT || 8000;

app.listen(PORT, () => console.log(`server started on port ${PORT}`));
