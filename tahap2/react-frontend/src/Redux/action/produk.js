import axios from "axios";
import { useNavigate } from "react-router-dom";

export const SET_LOADING = "produk/SET_LOADING";
export const SET_ERROR = "produk/SET_ERROR";
export const SET_DATA = "produk/SET_DATA";
export const SET_DETAIL = "produk/SET_DETAIL";
export const SET_STATUS = "prdouk/SET_STATUS";

export const setLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

export const setError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setData = (payload) => ({
  type: SET_DATA,
  payload,
});

export const setDetail = (payload) => ({
  type: SET_DETAIL,
  payload,
});

export const setStatus = (payload) => ({
  type: SET_STATUS,
  payload,
});

export const getAllDataProduct = () => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const response = await axios.get(`http://localhost:8000/produk`);
    dispatch(setData(response.data));
    dispatch(setLoading(false));
  } catch (error) {
    dispatch(setLoading(false));
    dispatch(setModal(true));
    dispatch(setError({ error: true, message: error.message }));
  }
};

export const getDetailProduct = (id) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const response = await axios.get(`http://localhost:8000/produk/${id}`);
    dispatch(setDetail(response.data));
    dispatch(setLoading(false));
  } catch (error) {
    dispatch(setLoading(false));
    dispatch(setError({ error: true, message: error.message }));
  }
};

export const deleteProduct = (id) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const response = await axios.delete(`http://localhost:8000/produk/${id}`);
    dispatch(setStatus(response.status));
    dispatch(setLoading(false));
  } catch (error) {
    dispatch(setLoading(false));
    dispatch(setError({ error: true, message: error.message }));
  }
};

export const addProduct = (payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const response = await axios.post(`http://localhost:8000/produk/`, payload);
    // dispatch(setStatus(response.status));
    dispatch(setLoading(false));
  } catch (error) {
    dispatch(setLoading(false));
    dispatch(setError({ error: true, message: error.message }));
  }
};

export const updateProduct = (id, payload) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const response = await axios.put(
      `http://localhost:8000/produk/${id}`,
      payload
    );
    dispatch(setStatus(response.status));
    dispatch(setLoading(false));
  } catch (error) {
    dispatch(setLoading(false));
    dispatch(setError({ error: true, message: error.message }));
  }
};
