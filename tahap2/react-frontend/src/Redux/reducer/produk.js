import {
  SET_DATA,
  SET_ERROR,
  SET_LOADING,
  SET_DETAIL,
  SET_STATUS,
} from "../action/produk";

const initialState = {
  isLoading: false,
  data: [],
  detail: "",
  status: false,
  error: { isError: false, message: "" },
};

export default (state = initialState, { payload, type }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: payload,
      };
    case SET_ERROR:
      return {
        ...state,
        error: payload,
      };
    case SET_DATA:
      return {
        ...state,
        data: payload,
      };
    case SET_DETAIL:
      return {
        ...state,
        detail: payload,
      };
    case SET_STATUS:
      return {
        ...state,
        status: payload,
      };
    default:
      return state;
  }
};
