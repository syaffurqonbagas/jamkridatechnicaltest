import { combineReducers } from "redux";
import produk from "./produk";

export default combineReducers({
  produk,
});
