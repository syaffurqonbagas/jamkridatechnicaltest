import React from "react";
import NewPages from "./NewPages";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import NewDetail from "./NewPages/NewDetail";

function App() {
  return (
    <>
      <Router>
        <Routes>
          <Route path="/" element={<NewPages />} />
          <Route path="/detail/:id" element={<NewDetail />} />
        </Routes>
      </Router>
    </>
  );
}

export default App;
