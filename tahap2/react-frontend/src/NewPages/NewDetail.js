import React from "react";
import { useParams } from "react-router-dom";
import {
  getDetailProduct,
  deleteProduct,
  setStatus,
} from "../Redux/action/produk";
import NavbarProduk from "../Components/Navbar";
import { useSelector, shallowEqual, useDispatch } from "react-redux";
import { useEffect } from "react";
import { css, cx } from "@emotion/css";
import { Modal, Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import FormModal from "../Components/Form/Form";
import ModalConfirm from "../Components/ModalConfirm";

const NewDetail = () => {
  const { error, isloading, data, detail, status } = useSelector(
    ({ produk }) => ({
      error: produk.error,
      isloading: produk.isloading,
      data: produk.data,
      detail: produk.detail,
      status: produk.status,
    }),
    shallowEqual
  );

  const dispatch = useDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    dispatch(getDetailProduct(params.id));
  }, []);

  useEffect(() => {
    if (status === 200) {
      navigate("/");
      dispatch(setStatus(""));
      setModalKonfirm(false);
    }
  }, [status]);

  const dataDetail = detail || "";
  // const gambar = dataDetail?.gambar[0]?.urlImage || [];
  // console.log(gambar);

  const [modalUpdate, setModalUpdate] = React.useState(false);
  const [modalKonfirm, setModalKonfirm] = React.useState(false);

  console.log();
  const params = useParams();

  return (
    <div className="">
      <NavbarProduk back />
      <div className="container">
        <div
          className={css`
    filter: blur(4px)
      display: flex;
      align-items: end;
      height: 380px;
      width: 100%;
      background-repeat: no-repeat;
      background-size: cover;
      background-repeat: no-repeat;
      background-position: center center;
      filter: blur(3px);
    `}
        ></div>
        <div
          className={css`
            display: flex;
            flex-direction: column;
            position: relative;
            bottom: 165px;
            margin-left: 20px;
          `}
        >
          <div
            className={css`
              display: flex;
              justify-content: space-between;
              align-items: center;
            `}
          >
            <h1
              className={css`
                color: #ffff00;
              `}
            >
              {detail.nama}
            </h1>
            <div>
              <button
                type="button"
                className="btn btn-primary h-25 me-3"
                onClick={() => setModalUpdate(true)}
              >
                Update
              </button>
              <button
                type="button"
                className="btn btn-danger h-25"
                onClick={() => setModalKonfirm(true)}
              >
                Delete
              </button>
            </div>
          </div>

          <h2
            className={css`
              color: white;
            `}
          >
            {detail.deskripsi}
          </h2>
        </div>
      </div>
      <FormModal
        jenis="update"
        show={modalUpdate}
        id={params.id}
        onHide={() => setModalUpdate(false)}
      />
      <ModalConfirm
        show={modalKonfirm}
        onHide={() => setModalKonfirm(false)}
        fungsi={() => {
          setModalKonfirm(false);
          dispatch(deleteProduct(params.id));
        }}
        deskripsi="Data Barang akan permanen di hapus apakah anda yakin ingin menghapus
        barang ?"
      />
    </div>
  );
};

export default NewDetail;
