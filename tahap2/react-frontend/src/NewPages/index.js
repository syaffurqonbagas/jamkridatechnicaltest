import React, { useEffect } from "react";
import { shallowEqual, useSelector } from "react-redux";
import { useState } from "react";
import Card from "../Components/Card";
import { useNavigate } from "react-router-dom";
import NavbarProduk from "../Components/Navbar";
import { getAllDataProduct } from "../Redux/action/produk";
import { useDispatch } from "react-redux";
import Loading from "../Components/Loading";
import FormModal from "../Components/Form/Form";

const NewPages = () => {
  const { error, isloading, data } = useSelector(
    ({ produk }) => ({
      error: produk.error,
      isloading: produk.isloading,
      data: produk.data,
    }),
    shallowEqual
  );
  const [modalUpdate, setModalUpdate] = React.useState(false);
  const [filter, setFilter] = React.useState("");
  const [search, setSearh] = useState("");

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleKeyDown = (event) => {
    if (event.key === "Enter") {
      setFilter("search");
      console.log("masuk");
    }
  };

  const goDetail = (id) => {
    navigate(`/detail/${id}`);
  };

  useEffect(() => {
    dispatch(getAllDataProduct());
  }, []);

  console.log("ini isi datanya,", search);
  return (
    <>
      {" "}
      <NavbarProduk plus onClick={() => setModalUpdate(true)} />
      <div
        className="container d-flex justify-content-lg-start justify-content-center w-100"
        style={{ marginTop: "100px" }}
      >
        <button
          type="button"
          class="btn btn-warning me-4"
          onClick={() => {
            setFilter("");
          }}
        >
          Semua
        </button>
        {data?.map((item, index) => {
          return (
            <button
              key={index}
              type="button"
              class="btn btn-warning me-4"
              onClick={() => {
                setFilter(item.kategori);
              }}
            >
              {item.kategori}
            </button>
          );
        })}
        <div class="input-group ">
          <input
            type="text"
            class="form-control"
            placeholder="Search"
            aria-label="Search"
            aria-describedby="basic-addon2"
            onChange={(e) => setSearh(e.target.value)}
            onKeyDown={handleKeyDown}
          ></input>
          <button class="input-group-text" id="basic-addon2">
            Search
          </button>
        </div>
      </div>
      <div className="container d-flex flex-wrap justify-content-lg-start justify-content-center my-5">
        {filter !== "" &&
          data
            ?.filter((items) => {
              return items.kategori == filter;
            })
            .map((item, index) => (
              <Card
                key={index}
                onClick={() => goDetail(item.id)}
                title={item?.nama}
                subTitle={item?.kategori}
                image={item?.gambar[0]?.urlImage}
              />
            ))}
        {filter === "" &&
          data?.map((item, index) => (
            <Card
              key={index}
              onClick={() => goDetail(item.id)}
              title={item?.nama}
              subTitle={item?.kategori}
              image={item?.gambar[0]?.urlImage}
            />
          ))}
        <FormModal show={modalUpdate} onHide={() => setModalUpdate(false)} />
      </div>
    </>
  );
};

export default NewPages;
