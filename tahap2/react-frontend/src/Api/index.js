import axios from "axios"

export const getAnimeData = (limit,page) => axios.get(`https://kitsu.io/api/edge/anime?page[limit]=${limit}&page[offset]=${page}`)