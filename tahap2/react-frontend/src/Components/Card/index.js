import Card from "react-bootstrap/Card";
import { css, cx } from "@emotion/css";
import { useNavigate } from "react-router-dom";
import dummy from "../../Image/dummy.jpg";

function CardAnime(props) {
  const navigate = useNavigate();
  const { title, subTitle, image, category, onClick } = props;

  return (
    <Card
      onClick={onClick}
      className={css`
        margin: 10px;
        background: transparent;
        color: white;
        &:hover {
          transform: scale(1.04);
          transition: all 0.5s ease;
        }
      `}
      style={{ width: "18rem" }}
    >
      <Card.Img variant="top" src={image || dummy} />
      <Card.Body>
        <Card.Title
          className={css`
            font-size: 24px;
            color: #ffff00;
          `}
        >
          {title}
        </Card.Title>
        <Card.Subtitle
          className={css`
            margin-top: 5px;
            font-size: 18px;
          `}
        >
          {subTitle}
        </Card.Subtitle>
        <Card.Text>{category}</Card.Text>
      </Card.Body>
    </Card>
  );
}

export default CardAnime;
