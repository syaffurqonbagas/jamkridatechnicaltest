const Loading = (state) => {
  return (
    <div
      className={css`
        display: flex;
        justify-content: center;
        align-items: center;
        height: 100vh;
      `}
    >
      <PacmanLoader size={40} loading={state} color=" #FFFF00" />
    </div>
  );
};

export default Loading;
