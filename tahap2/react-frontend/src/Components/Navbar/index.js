import { Navbar, Container } from "react-bootstrap";
import PacmanLoader from "react-spinners/PacmanLoader";
import { css, cx } from "@emotion/css";
import { IoMdArrowRoundBack } from "react-icons/io";
import { GoDiffAdded } from "react-icons/go";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { setStatus } from "../../Redux/action/produk";

function NavbarProduk(props) {
  const { back, plus, onClick } = props;
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const goBack = () => {
    dispatch(setStatus(""));
    navigate(`/`);
  };
  return (
    <Navbar
      className={css`
        z-index: 9;
        position: fixed;
        top: 0px;
        width: 100%;
        background-color: #ffff00;
      `}
    >
      <Container>
        <Navbar.Brand>
          <PacmanLoader
            size={10}
            color="black"
            className={css`
              margin-bottom: 7px;
            `}
          />
        </Navbar.Brand>
        {back && (
          <div>
            <IoMdArrowRoundBack
              className={css`
                transition: transform 0.2s;
                &:hover {
                  transform: scale(1.5);
                }
              `}
              size={25}
              onClick={() => goBack()}
            />
          </div>
        )}
        {plus && (
          <div
            onClick={onClick}
            className={css`
              display: flex;
              align-items: center;
              transition: transform 0.2s;
              &:hover {
                transform: scale(1.1);
                cursor: default;
              }
            `}
          >
            <GoDiffAdded size={25} />
            <h5 className="m-2">Add Product</h5>
          </div>
        )}
      </Container>
    </Navbar>
  );
}

export default NavbarProduk;
