import React from "react";
import { Modal, Button } from "react-bootstrap";
import { useDispatch } from "react-redux";
import { deleteProduct } from "../../Redux/action/produk";

function ModalConfirm(props) {
  const dispatch = useDispatch();

  return (
    <Modal
      show={props.show}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">Konfirmasi</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <p>{props.deskripsi}</p>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={props.fungsi}>Iya</Button>
        <Button className="btn-danger" onClick={props.onHide}>
          Tidak
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

export default ModalConfirm;
