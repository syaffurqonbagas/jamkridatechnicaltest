import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import Button from "react-bootstrap/Button";
import { addProduct, updateProduct } from "../../Redux/action/produk";
import Modal from "react-bootstrap/Modal";
import { Form, Field } from "react-final-form";
import ModalConfirm from "../ModalConfirm";

function FormModal(props) {
  const dispatch = useDispatch();
  const [payload, setPayload] = React.useState({});
  const [modalKonfirm, setModalKonfirm] = React.useState(false);

  function refreshPage() {
    window.location.reload(false);
  }

  const onSubmit = async (values) => {
    setPayload({
      nama: values?.nama,
      kategori: values?.kategori,
      deskripsi: values?.deskripsi,
      warna: values?.warna,
      variant: [
        { harga_ukuran_S: values?.harga_ukuran_S ?? 0 },
        { harga_ukuran_M: values?.harga_ukuran_M ?? 0 },
        { harga_ukuran_L: values?.harga_ukuran_L ?? 0 },
        { harga_ukuran_XL: values?.harga_ukuran_XL ?? 0 },
        { harga_ukuran_XLL: values?.harga_ukuran_XLL ?? 0 },
      ],
    });
  };

  // const confirm = ;

  useEffect(() => {
    console.log(payload);
  }, [payload]);
  const required = (value) => (value ? undefined : "Required");

  return (
    <>
      <Modal {...props}>
        <Modal.Header closeButton>
          <Modal.Title>Modal heading</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form
            onSubmit={onSubmit}
            initialValues={props.initialValues}
            render={({ handleSubmit, form, submitting, pristine, values }) => (
              <form onSubmit={handleSubmit}>
                <div className="mb-3">
                  <Field
                    validate={required}
                    name="nama"
                    render={({ input, meta }) => (
                      <div>
                        <label htmlFor="nama" className="form-label">
                          Nama Barang
                        </label>
                        <input
                          {...input}
                          className={
                            meta.touched && meta.error
                              ? "form-control is-invalid"
                              : "form-control"
                          }
                        />
                        {meta.touched && meta.error && (
                          <span className="invalid-feedback">{meta.error}</span>
                        )}
                      </div>
                    )}
                  />
                </div>
                <div className="mb-3">
                  <label htmlFor="kategori" className="form-label">
                    Kategori
                  </label>
                  <Field name="kategori" component="select" validate={required}>
                    {({ input, meta }) => (
                      <div className={meta.active && "active"}>
                        <select className="form-select" {...input}>
                          <option /> <option value="Pakaian">Pakaian</option>
                          <option value="Celana">Celana</option>
                          <option value="Topi">Topi</option>
                          <option value="Jas">Jas</option>
                          <option value="Sepatu">Sepatu</option>
                          <option value="Jaket">Jaket</option>
                          <option value="Tas">Tas</option>
                          <option value="Dompet">Dompet</option>
                        </select>
                        {meta.touched && meta.error && (
                          <span className="invalid-feedback">{meta.error}</span>
                        )}
                      </div>
                    )}
                  </Field>
                </div>
                <div className="mb-3">
                  <Field
                    validate={required}
                    name="deskripsi"
                    render={({ input, meta }) => (
                      <div>
                        <label htmlFor="nama" className="form-label">
                          Deskripsi
                        </label>
                        <textarea
                          {...input}
                          className={
                            meta.touched && meta.error
                              ? "form-control is-invalid"
                              : "form-control"
                          }
                        />
                        {meta.touched && meta.error && (
                          <span className="invalid-feedback">{meta.error}</span>
                        )}
                      </div>
                    )}
                  />
                </div>

                <div className="mb-3">
                  <label>Warna</label>
                  <div>
                    <label className="form-check- me-3">
                      <Field
                        name="warna"
                        component="input"
                        type="checkbox"
                        className="form-check-input"
                        value="Merah"
                      />{" "}
                      Merah
                    </label>
                    <label className="form-check-label me-3">
                      <Field
                        name="warna"
                        component="input"
                        type="checkbox"
                        className="form-check-input"
                        value="Biru"
                      />{" "}
                      Biru
                    </label>
                    <label className="form-check-label me-3">
                      <Field
                        name="warna"
                        component="input"
                        type="checkbox"
                        className="form-check-input"
                        value="Hitam"
                      />{" "}
                      Hitam
                    </label>
                    <label className="form-check-label me-3">
                      <Field
                        name="warna"
                        component="input"
                        type="checkbox"
                        className="form-check-input"
                        value="Abu-abu"
                      />{" "}
                      Abu-abu
                    </label>
                  </div>
                </div>
                <div className="mb-3">
                  <label>Ukuran</label>
                  <div>
                    <label className="form-check-label me-3">
                      <Field
                        validate={required}
                        name="ukuran"
                        component="input"
                        type="checkbox"
                        className="form-check-input"
                        value="S"
                      />{" "}
                      S
                    </label>
                    <label className="form-check-label me-3">
                      <Field
                        validate={required}
                        name="ukuran"
                        component="input"
                        type="checkbox"
                        className="form-check-input"
                        value="M"
                      />{" "}
                      M
                    </label>
                    <label className="form-check-label me-3">
                      <Field
                        validate={required}
                        name="ukuran"
                        component="input"
                        type="checkbox"
                        className="form-check-input"
                        value="L"
                      />{" "}
                      L
                    </label>
                    <label className="form-check-label me-3">
                      <Field
                        validate={required}
                        name="ukuran"
                        component="input"
                        type="checkbox"
                        className="form-check-input"
                        value="XL"
                      />{" "}
                      XL
                    </label>
                    <label className="form-check-label me-3">
                      <Field
                        validate={required}
                        name="ukuran"
                        component="input"
                        type="checkbox"
                        className="form-check-input"
                        value="XLL"
                      />{" "}
                      XLL
                    </label>
                  </div>
                </div>

                {values.ukuran &&
                  values.ukuran.map((item, idx) => {
                    return (
                      <div key={idx} className="mb-3">
                        <Field
                          validate={required}
                          name={`harga_ukuran_${item}`}
                          render={({ input, meta }) => (
                            <div>
                              <label
                                htmlFor="hargabarang"
                                className="form-label"
                              >
                                {`Harga Barang ${item}`}
                              </label>
                              <input
                                {...input}
                                className={
                                  meta.touched && meta.error
                                    ? "form-control is-invalid"
                                    : "form-control"
                                }
                              />
                              {meta.touched && meta.error && (
                                <span className="invalid-feedback">
                                  {meta.error}
                                </span>
                              )}
                            </div>
                          )}
                        />
                      </div>
                    );
                  })}
                <Modal.Footer>
                  <Button variant="secondary" onClick={props.onHide}>
                    Close
                  </Button>
                  <Button
                    type="submit"
                    variant="primary"
                    onClick={() => setModalKonfirm(true)}
                    disabled={submitting || pristine}
                  >
                    Save Changes
                  </Button>
                </Modal.Footer>
              </form>
            )}
          />
        </Modal.Body>
      </Modal>
      <ModalConfirm
        show={modalKonfirm}
        onHide={() => setModalKonfirm(false)}
        fungsi={() => {
          setModalKonfirm(false);
          if (props.jenis === "update") {
            dispatch(updateProduct(props.id, payload));
          } else {
            dispatch(addProduct(payload));
            refreshPage();
          }
        }}
        deskripsi="Apakah anda yakin ingin menambahkan 
        barang ini ?"
      />
    </>
  );
}

export default FormModal;
